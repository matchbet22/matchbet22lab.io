# Equipe

## Wanderley Abreu Junior (CEO)
<img src="/images/Equipe-Nova-5-2.png" alt="Wanderley Abreu Junior" width="200"/>

CEO da Storm Group. Engenheiro Mecatrônico pela PUC-Rio. Pós-graduação pela Massachusetts Institute of Tecnology - EUA. Especialista em sistemas críticos com mais de 20 anos de atuação em instituições como a NASA, Agência Espacial Europeia – ESA e Agência Espacial Canadense. No final dos anos 90, em trabalho conjunto com o Promotor de Justiça Romero Lyra, desenvolveu, atuou e aperfeiçoou a primeira Promotoria de Justiça de Investigações Eletrônicas do Brasil, em que acabou sendo responsável pela identificação de mais de 200 pedófilos na primeira operação contra a pedofilia online do Brasil – Operação Catedral. Professor e Instrutor do Ciclo de Estudos de Capacitação na Prevenção e Enfrentamento das Ameaças Assimétricas e NBQRE. Condecorado com a Medalha Tiradentes pela Assembleia Legislativa do Estado do Rio de Janeiro -2021

## Antonio Carlos Castanon (COO)
<img src="/images/Equipe-Nova-4-2.png" alt="Antonio Carlos Castanon" width="200"/>

Graduado em Engenharia Eletrônica pelo Instituto Militar de Engenharia - IME (1989), mestrado em Engenharia Elétrica pela Universidade Federal do Rio de Janeiro (1999) e doutorado em Engenharia Elétrica pela Universidade Federal do Rio de Janeiro (2005). Tem experiência em gestão de projetos, em especial na área de Tecnologia da Informação e Comunicações (TIC), atuando por cerca de 20 anos, no assessoramento no Centro Tecnológico do Exército (CTEx), na execução de atividades de Pesquisa e Desenvolvimento (P&D) e na coordenação/gestão de projetos: Comando e Controle (C2), Guerra Eletrônica (GE), sistemas radar, simuladores de armas e de sistemas de armas e segurança da informação. Professor e coordenador em cursos de graduação e pós-graduação, em particular Engenharia Elétrica e Engenharia de Segurança do Trabalho na Universidade Estácio de Sá. Atualmente, se dedica à STORM, como Diretor de Operações, em atividades e Projetos na área de Tecnologia da Informação.

## Luan Garcia (CTO)
<img src="/images/Equipe-Nova-3-2.png" alt="Luan Garcia" width="200"/>

Graduado em Gestão de Tecnologia da Informação e especialista em desenvolvimento Frontend. Possui 15 anos de experiência em gestão de equipes, desenvolvimento de software e plataformas de vídeo. Atualmente é CTO da StormGroup, onde é responsável pelo setor de Tecnologia.

## Marcos Lima (Diretor de Pesquisa e Desenvolvimento)
<img src="/images/Equipe-Nova-2-2.png" alt="Marcos Lima" width="200"/>

Formado em física pela Universidade de São Paulo – USP e programador sênior há mais de 10 anos. Já trabalhou em grandes empresas como a Ultrafarma. Atualmente exerce o cargo de gerente de pesquisa e desenvolvimento na StormGroup. Possui ampla experiência em visão computacional e programação backend.

## Ana Medrado (Product Designer)
<img src="/images/Equipe-Nova-1-2.png" alt="Ana Medrado" width="200"/>

Bacharel em sistemas de informação, mestranda em computação aplicada na federal tecnológica do Paraná. Ampla experiência em desenvolvimento de produtos tecnológicos, com ênfase em design e usabilidade. Atualmente Product Designer na Arena.im, palestrante e influenciadora digital da área tecnológica.

## Matheus Falcão (Coordenador do Projeto)
<img src="/images/Equipe-Nova-6-1.png" alt="Matheus Falcão" width="200"/>

Graduado em Ciência da Computação pela PUC-Rio. Participou da 1ª turma do Apple Developer Academy onde aprendeu a desenvolver aplicativos para iOS. Tem experiência com gerenciamento de projetos, modelagem de dados, desenvolvimento mobile e backend. Atualmente é CEO da Noclaf Tech, onde já foram desenvolvidos diversos projetos em diferentes áreas e tecnologias.
