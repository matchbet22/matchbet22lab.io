# Tokenomics da Matchcoin
A Matchcoin ($MATCH) é o criptoativo que será usado para todas as transações no site MatchBet. É o componente essencial de toda a estrutura financeira da plataforma e, externamente, é o ativo que sofre variações no mercado e pode ser transacionado. A Matchcoin será listada nas maiores corretoras de criptoativos do mundo.

## Pré-Venda
A MatchBet disponibilizará 20% de suas Matchcoins para serem adquiridas nas fases de Pré-Venda. Elas serão distribuídas da seguinte forma:

<img width="800" src="/images/prevenda_11-04-2022.jpg" alt="">

Obviamente, levando apenas o preço da moeda em consideração, a PrivateSale é onde se encontra a Matchcoin com o preço mais atrativo. Entretanto, o percentual disponibilizado aos investidores é menor.
## Quadro de Pré-Venda e Alocação de Matchcoins detalhado:

| Descrição | Valor |
| ------ | ------ |
|Valores Token Private  |	 $0,012 |
|Valores Token Private 2|	 $0,014 |
|Valores Token pivate 3	| $0,016 | 
|Valores Token pivate 3	| $0,018 | 
|Valores Token Lancamento	| $0,024 | 

| Descrição | Valor |
| ------ | ------ |
|Valor MATIC (07/01/2023)	| $0,80 | 
|Supply Total	|  100.000.000 | 
|Tokens disponíveis para pré venda	| 32.000.000 |

**SoftCap para lançamento**

| Descrição | Valor |
| ------ | ------ |
| SOFT CAP (Tokens)	| 5.000.000 
| Valor Softcap USD	| 60.000,00 
| Valor Softcap MATIC	| 75.000,00 

**HardCap para lançamento**

| Descrição | Valor |
| ------ | ------ |
| HARD CAP (Tokens)	| 32.000.000 
| Valor hardcap USD	| 384.000,00 
| Valor hardcap MATIC	| 480.000,00 

   
**Previsão lançamento**

| Descrição | Valor |
| ------ | ------ |
| Percentual arrecadação Liquidez	| 10,00% | 
| Percentual arrecadação SoftCap	|  5,00% | 
| Percentual arrecadação HardCap	|  32,00% |


A liquidez poderá ser aumentada até o lançamento do projeto, a depender da arrecadação final. Tokens não vendidos serão destinados para a **MATCHDEX (DEX Própria do projeto)**, com mais adição de liquidez, para isso serão utilizados tokens restantes da pre venda, equivaentes a 33% do supply, conforme indicado no tokenomics.

## Tokenomics 

| Descrição | Tokens | USD |
| ------ | ------ | ------ |
|10% Liquidez	| 10.000.000 |  $240.000,00 |
|3% Mkt    	|   3.000.000 |   $72.000,00 |
|3% Advisor	|   3.000.000 |	 $72.000,00 |
|8% Dev	      |  8.000.000 |	 $192.000,00 |
|8% Reinvestimento Projeto	| 8.000.000 	| $192.000,00 |
|3% Airdrop	|  3.000.000 	| $72.000,00 |
|15% Vendas Privadas	|  15.000.000 |	 $360.000,00 |
|12% ICO	|  12.000.000 	|  $288.000,00 |
|5% Pré venda	|  5.000.000 |	 $120.000,00 |
|33% Outras DEX | 	 33.000.000 |	 $792.000,00 |

## Public Sale/TGE
<img width="620" src="/images/publicsale_20-12-2022.jpeg" alt="">

Observação importante: Caso não sejam vendidas todas as Matchcoins nas etapas de Pré-venda e ICO, as Matchcoins que restarem sofrerão um Burn. Dessa forma, vamos beneficiar aqueles que acreditaram no projeto e não vamos dar chances aqueles que ficam aguardando nas Exchanges. Em outras palavras, as etapas de Pré-venda serão o melhor momento de compra a um valor reduzido. 

Após o período de Pré-Venda e ICO, a Matchcoin será listada ao público em geral nas corretoras de criptoativos.


## Airdrop

Link para o BOT de AIRDROP: https://hi.switchy.io/8dcf

Você poderá participar do nosso lançamento indicando amigos, realizando tarefas e ganhando por cada tarefa realizada.

Informações: 

<ul>
<li>Pool PRE-ICO: 20,000,000 $Matchcoin; </li>
<li>Pool Pós-ICO: 80,000,000 $Matchcoin;</li>
<li>Atividade de cadastro: 5 $Matchcoin por usuário; </li>
<li>Atividade de indicação: 30 $Matchcoin por usuário; </li>
<li>Top 1,000 indicadores: 5,600,000 $Matchcoin;</li>
<li>Fim da campanha PRE-ICO: 20 de outubro de 2022;</li>
<li>Distribuição da campanha PRE-ICO: 15 de novembro de 2022;</li>
</ul>


Airdrop consiste em entregar prêmios de tokens utilitários a quem participar da comunidade do MatchBet e efetuar tarefas sociais em favor do produto. A estratégia de Airdrop entra como um recurso no campo de ações de marketing.

O total maximo destinado a esta mecânica é de 1% dos tokens. Se acaso o valor total de tokens a serem distribuidos ultrapassar este total, o valor individual destinado a cada usuário irá decrescer progressivamente e propoporcionalmente. Se acaso sobrarem tokens, o valor retornará para a wallet de marketing da empresa, reserva-se no livre direito de uso em futuros Airdrops ou outras ações.s

### Regras
1. Ao participar da comunidade do Telegram, o usuário ganha um valor em tokens como premiação.
   * Planeja-se empreender em tokens o valor inicial de aproximadamente 2000 Matchcoins (~50BUSD) como premiação apenas por participar do Airdrop. Podendo alcançar valores maiores caso o usuário efetue tarefas sociais como compartilhamento em redes sociais ou referencie outros usuários.
2. A cada tarefa executada, adiciona-se um valor a mais em seu saldo de prêmios. As tarefas podem ser indicação de outros usuários ou participação em outros canais indicados.
3. Ao termino do periodo do Airdrop, a empresa irá entregar via airdrop o saldo atual de tokens diretamente na carteira do usuário cadastrado no BOT.
- Não é necessário o KYC para receber os tokens bonificados.
- As pessoas a quem você indica, devem participar de todas as tarefas de airdrop para serem contadas como uma indicação válida.
- A quantidade de pessoas indicadas por alguem é ilimitada.
- Contas múltiplas ou falsas não são permitidas e serão eliminadas.
- Spamming no grupo do Telegram não é permitido e não será tolerado.
- Se seus dados enviados estiverem errados, você poderá reenviar os dados novamente antes que o airdrop termine;
- Você deve continuar a seguir as tarefas das quais participou até que a distribuição do airdrop seja concluída;


Regras de bonificação para os maiores promotores da MatchBet, de acordo com sua posição no ranking: 
- Da 1º a 10º posição = 200,000 $ Matchcoin cada 
- Da 11º a 100º posição = 20,000 $ Matchcoin cada
- Da 101º a 1000º posição = 2,000 $ Matchcoin cada

Para quaisquer dúvidas entre em contato através de nosso canal no telegram: 
https://t.me/matchbetprevenda

## Fees and Temp-Bans
A empresa se reserva no direito de poder aplicar fees nas transações e o contrato impede que a empresa configure fees que o total ultrapasse 10%.

### Reflection fee
- Um valor de toda transação é removido e distribuido proporcionalmente a todos os holders. [Reference](https://www.axi.com/int/blog/education/blockchain/reflection-tokens)

### Staking fee
- Uma proporção de fees é removida para pagar incentivos de staking de maneira customizada. A empresa se reserva no direito de descrever como serão estes incentivos de staking em um contrato futuro.

### Ecosystem fee (community fund)
- Fee que a empresa recebe para sustentar e manter suas operações a longo prazo. Podendo ser usado com discricionariedade pela empresa.

### Burn fee
- Fee que causa escassez de moeda. Uma porção dos tokens são queimados causando deflação e beneficiando holders que nao transacionam frequentemente.

### Liquidity fee
- Parte das fees serão usadas para adicionar liquidez na pool.
1. Metade dos tokens de liquidity fee são vendidos a mercado na DEX e convertidos no outro elemento do DexPair. Neste momento, 50% do fee original se encontra convertido no outro elemento do par e 50% permanece no token original;
2. Adiciona-se liquidez na DEX usando os valores acima;
3. A empresa pode escolher ser a detentora dos comprovantes do depósito na DEX ou devolver eles aos clientes, na forma de cashback, incentivando que eles façam staking na DEX e recebam dividendos diretamente da DEX.

### AntiBot TimeLock DEX
- Para evitar a existência de bots, e desestimular sua atuação, cada wallet depois de uma operação em dex será banida temporariamente. O limite máximo que o contrato permite que a empresa aplique é de 1 dia.
